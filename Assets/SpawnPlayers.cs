﻿using UnityEngine;

public class SpawnPlayers : MonoBehaviour {

    public Transform Swimmer;
    public Transform Bubble;

    public void Spawn()
    {
        Swimmer.gameObject.SetActive(true);
        Bubble.gameObject.SetActive(true);
    }
}
