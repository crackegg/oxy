﻿using UnityEngine;

public class AnimateToRight : MonoBehaviour
{
    public RectTransform CanvasToAnimate;

    public float Delay = 0;

    [Range(0, 400)]
    public float Velocity = 0.5f;

    [Range(0, 100)]
    public float Acceleration = 0.01f;
    [Range(1, 2)]
    public float DeccelerationRatio = 1.1f;

    private float _offsetLeft = -250;
    private float _cooldown;
    private float _currentVelocity;
    private float _currentAcceleration;
    private bool _isDeccelaring;

    public void Start ()
    {
        Init();
    }
	
	public void Update ()
	{
	    if (Time.time <= _cooldown) return;

	    if (IsBackToItsPosition())
	    {
	        CanvasToAnimate.offsetMin = new Vector2(0, CanvasToAnimate.offsetMin.y);
            return;
	    }

	    if (_offsetLeft >= 0)
	    {
	        _currentAcceleration *= DeccelerationRatio;
	        _offsetLeft += _currentVelocity * Time.deltaTime;
	        _currentVelocity -= _currentAcceleration;
	    }
	    else if (_offsetLeft < 0)
	    {
	        _offsetLeft += _currentVelocity * Time.deltaTime;
	        _currentVelocity += _currentAcceleration;
	    }
	    else
	        return;

	    CanvasToAnimate.offsetMin = new Vector2(_offsetLeft, CanvasToAnimate.offsetMin.y);
    }

    public void Init()
    {
        _cooldown = Time.time + Delay;
        _offsetLeft = -250;
        CanvasToAnimate.offsetMin = new Vector2(_offsetLeft, CanvasToAnimate.offsetMin.y);
        _currentAcceleration = Acceleration;
        _currentVelocity = Velocity;
    }

    public void OnEnable()
    {
        Init();
    }

    private bool IsBackToItsPosition()
    {
        return _currentVelocity <= 0 && _offsetLeft <= 0;
    }
}
