﻿using UnityEngine;
using UnityEngine.UI;

public class FadeInFadeOut : MonoBehaviour {

    public Image Image;
    public float Time;
    
    public void Start()
    {
        FadeOut();
    }

    private void FadeIn()
    {
        Image.CrossFadeAlpha(1f, Time, false);
        Invoke("FadeOut", Time);
    }

    private void FadeOut()
    {
        Image.CrossFadeAlpha(0f, Time, false);
        Invoke("FadeIn", Time);
    }
}
