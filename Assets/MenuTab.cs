﻿using UnityEngine;

namespace Assets
{
    public abstract class MenuTab : MonoBehaviour
    {
        public GameObject Active;

        public abstract void OnClick();

        public void Select()
        {
            Active.SetActive(true);
        }

        public void Deselect()
        {
            Active.SetActive(false);
        }
    }
}
