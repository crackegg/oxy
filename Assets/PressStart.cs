﻿using UnityEngine;

public class PressStart : MonoBehaviour {

    public Transform MenuButtons;
    public Transform Title;
    
	public void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            MenuButtons.gameObject.SetActive(true);
            Title.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
