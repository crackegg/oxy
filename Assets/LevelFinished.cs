﻿using Assets.Scripts;
using Assets.Scripts.Manager;
using Assets.Scripts.State.Swimmers;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelFinished : MonoBehaviour {

    public Swimmer Swimmer;
    public Bubble Bubble;
    public ProCamera2DTransitionsFX EndTransition;
    public Transform Interface;
    public OxygenManager OxygenManager;
    public GameManager GameManager;

    public int NextLevelId;

    private BoxCollider2D _boxCollider;

    public void Start()
    {
        _boxCollider = GetComponent<BoxCollider2D>();
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Swimmer"))
        {
            _boxCollider.enabled = false;
            Interface.gameObject.SetActive(false);
            AkSoundEngine.PostEvent("LevelCompleted", gameObject);
            Win();
        }
    }

    private void Win()
    {
        Swimmer.StateMachine.GlobalState = WinnerSwimmerState.Instance();
        Swimmer.StateMachine.ChangeState(WinnerSwimmerState.Instance());
        EndTransition.TransitionExit();
        Invoke("Init", EndTransition.DurationExit - 0.1f);
        Invoke("SwitchToNextLevel", EndTransition.DurationExit);
    }

    private void Init()
    {
        OxygenManager.Initialize();
    }

    private void SwitchToNextLevel()
    {
        SceneManager.LoadScene(NextLevelId);
    }
}
