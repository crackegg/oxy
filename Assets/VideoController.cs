﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoController : MonoBehaviour
{
    public MovieTexture movie;

	public void Start ()
	{
	    GetComponent<RawImage>().texture = movie;
	    movie.loop = true;
        movie.Play();
	}
}
