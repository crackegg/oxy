﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PressAnyKeyLoadScene : MonoBehaviour {

    public FadeScreenManager FadeScreenManager;
    public int NextScene;

    public void Update()
    {
        if (Input.anyKeyDown)
        {
            //AkSoundEngine.PostEvent("StartLevel", gameObject);
            //FadeScreenManager.Instance.FadeIn(LoadScene);
        }
    }

    private void LoadScene()
    {
        SceneManager.LoadScene(NextScene);
    }
}
