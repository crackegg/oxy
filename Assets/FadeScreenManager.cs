﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadeScreenManager : MonoBehaviour {

    public static FadeScreenManager Instance;

    public Image Image;
    public Color Color;
    public float Time;

    public void Awake()
    {
        Image.color = Color;
        Instance = this;
        FadeOut(() => { });
    }

    public void FadeIn(Action callback)
    {
        Image.CrossFadeAlpha(1f, Time, false);
        StartCoroutine(CallbackWithDelay(callback));
    }

    public void FadeOut(Action callback)
    {
        Image.CrossFadeAlpha(0f, Time, false);
        StartCoroutine(CallbackWithDelay(callback));
    }

    private IEnumerator CallbackWithDelay(Action callback)
    {
        yield return new WaitForSeconds(Time + 0.5f);
        callback();
    }
}
