﻿using UnityEngine;

public class PressAnyKeyShowAndHide : MonoBehaviour
{
    public GameObject ElementToShow;
	
	public void Update () {
	    if (!Input.anyKeyDown) return;

	    ElementToShow.SetActive(true);
	    gameObject.SetActive(false);
	}
}
