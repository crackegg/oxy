#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System;


public class AkWwiseMenu_tvOS : MonoBehaviour {
#if !UNITY_5
	private static AkUnityPluginInstaller_tvOS m_installer = new AkUnityPluginInstaller_tvOS();

	// private static AkUnityIntegrationBuilder_tvOS m_builder = new AkUnityIntegrationBuilder_tvOS();

	[MenuItem("Assets/Wwise/Install Plugins/tvOS/Debug", false, (int)AkWwiseMenuOrder.tvOSDebug)]
	public static void InstallPlugin_Debug () {
		m_installer.InstallPluginByConfig("Debug");
	}

	[MenuItem("Assets/Wwise/Install Plugins/tvOS/Profile", false, (int)AkWwiseMenuOrder.tvOSProfile)]
	public static void InstallPlugin_Profile () {
		m_installer.InstallPluginByConfig("Profile");
	}

	[MenuItem("Assets/Wwise/Install Plugins/tvOS/Release", false, (int)AkWwiseMenuOrder.tvOSRelease)]
	public static void InstallPlugin_Release () {
		m_installer.InstallPluginByConfig("Release");
	}
#endif

//	[MenuItem("Assets/Wwise/Rebuild Integration/tvOS/Debug")]
//	public static void RebuildIntegration_Debug () {
//		m_builder.BuildByConfig("Debug", null);
//	}
//
//	[MenuItem("Assets/Wwise/Rebuild Integration/tvOS/Profile")]
//	public static void RebuildIntegration_Profile () {
//		m_builder.BuildByConfig("Profile", null);
//	}
//
//	[MenuItem("Assets/Wwise/Rebuild Integration/tvOS/Release")]
//	public static void RebuildIntegration_Release () {
//		m_builder.BuildByConfig("Release", null);
//	}
}


public class AkUnityPluginInstaller_tvOS : AkUnityPluginInstallerBase
{
	public AkUnityPluginInstaller_tvOS()
	{
		m_platform = "tvOS";
	}

	protected override string GetPluginDestPath(string arch)
	{
		return Path.Combine(m_pluginDir, m_platform);
	}	
}


public class AkUnityIntegrationBuilder_tvOS : AkUnityIntegrationBuilderBase
{
	public AkUnityIntegrationBuilder_tvOS()
	{
		m_platform = "tvOS";
	}
}
#endif // #if UNITY_EDITOR