﻿using UnityEngine;

namespace Assets
{
    public class BaseGameEntity<T> : MonoBehaviour
    {
        [HideInInspector]
        public StateMachine<T> StateMachine { get; set; }

        public long Id;
        public float MaxSpeed;

        [HideInInspector]
        public Rigidbody2D Rigidbody;
    }
}
