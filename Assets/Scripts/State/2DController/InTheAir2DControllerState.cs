﻿//using System;
//using UnityEngine;

//namespace Assets.Scripts.State._2DController
//{
//    public class InTheAir2DControllerState : State<Player>
//    {
//        const float GROUND_RADIUS = .2f;

//        private static InTheAir2DControllerState _instance;
        
//        public static InTheAir2DControllerState Instance()
//        {
//            if (_instance == null)
//                _instance = new InTheAir2DControllerState();

//            return _instance;
//        }

//        public override void Enter(Player entity)
//        {
//            entity.Animator.SetBool("Ground", false);
//            entity.Rigidbody.AddForce(new Vector2(0f, entity.JumpForce));
//        }

//        public override void Execute(Player entity)
//        {
//        }

//        public override void Exit(Player entity)
//        {
//        }

//        public override void FixedExecute(Player entity)
//        {
//            var colliders = Physics2D.OverlapCircleAll(entity.GroundCheck.position, GROUND_RADIUS, entity.WhatIsGround);
//            for (int i = 0; i < colliders.Length; i++)
//            {
//                if (colliders[i].gameObject != entity.gameObject)
//                    entity.MovementStateMachine.ChangeState(Grounded2DControllerState.Instance());
//            }
//        }
//    }
//}
