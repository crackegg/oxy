﻿//using System;
//using UnityEngine;

//namespace Assets.Scripts.State._2DController
//{
//    public class Global2DControllerState : State<Player>
//    {
//        private static Global2DControllerState _instance;
//        private float _horizontalMovement;

//        public static Global2DControllerState Instance()
//        {
//            if (_instance == null)
//                _instance = new Global2DControllerState();

//            return _instance;
//        }

//        public override void Enter(Player entity)
//        {
//        }

//        public override void Execute(Player entity)
//        {
//            entity.Animator.SetFloat("Speed", Mathf.Abs(_horizontalMovement));
//            entity.Rigidbody.velocity = new Vector2(_horizontalMovement * entity.MaxSpeed, entity.Rigidbody.velocity.y);

//            if (_horizontalMovement > 0 && !entity.FacingRight)
//            {
//                Flip(entity);
//            }
//            else if (_horizontalMovement < 0 && entity.FacingRight)
//            {
//                Flip(entity);
//            }
//        }

//        public override void Exit(Player entity)
//        {
//        }

//        public override void FixedExecute(Player entity)
//        {
//            _horizontalMovement = Input.GetAxis("Horizontal");
//            entity.Animator.SetFloat("vSpeed", entity.Rigidbody.velocity.y);
//        }

//        private void Flip(Player entity)
//        {
//            entity.FacingRight = !entity.FacingRight;

//            var scale = entity.transform.localScale;
//            scale.x *= -1;
//            entity.transform.localScale = scale;
//        }
//    }
//}
