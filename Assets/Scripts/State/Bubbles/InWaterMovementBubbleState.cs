﻿using UnityEngine;

namespace Assets.Scripts.State.Bubbles
{
    public class InWaterMovementBubbleState : State<Bubble>
    {
        public float UpwardMovementStrength = 5;

        private static InWaterMovementBubbleState _instance;

        private float _horizontalMovement;
        private float _fartPower;
        private float _deflateSpeed = 0.00f;
        private Vector2 _playerPosition;

        public static InWaterMovementBubbleState Instance()
        {
            if (_instance == null)
                _instance = new InWaterMovementBubbleState();
            
            return _instance;
            
        }

        public override void Enter(Bubble entity)
        {
        }

        public override void Execute(Bubble entity)
        {
            if (_horizontalMovement != 0f && !entity.IsOnCooldown)
            {
                AkSoundEngine.PostEvent("BubbleSwims", entity.gameObject);
                entity.StartCooldown();
            }
            entity.Rigidbody.AddForce(new Vector2(_horizontalMovement * entity.MaxSpeed * _fartPower * Time.deltaTime, 0));
            entity.OxygenManager.RemoveEnergyBubble(_deflateSpeed / 20.00f);
        }

        public override void Exit(Bubble entity)
        {
        }

        public override void FixedExecute(Bubble entity)
        {
            _horizontalMovement = Input.GetAxis("Horizontal");
            _deflateSpeed = Input.GetAxis("Fart");
            _fartPower = _deflateSpeed * 5.00f + 1.00f;

            var scale = (entity.OxygenManager.CurrentOxygenBubble * 0.005f) + 0.10f;
            entity.transform.localScale = new Vector2(scale, scale);
            entity.LeftEye.localScale = new Vector2(1, 1);
            entity.RightEye.localScale = new Vector2(1, 1);

            entity.Face.localPosition = new Vector2(0, 0);
            var ratio = (0.1f * _horizontalMovement);

            if (_horizontalMovement < 0)
            {
                entity.LeftEye.localScale = new Vector2(1f + ratio, 1f + ratio);
                entity.RightEye.localScale = new Vector2(1f - ratio, 1f - ratio);
                entity.Face.localPosition = new Vector2(_horizontalMovement, 0);
            }

            if (_horizontalMovement > 0)
            {

                entity.RightEye.localScale = new Vector2(1f + ratio, 1f + ratio);
                entity.LeftEye.localScale = new Vector2(1f - ratio, 1f - ratio);
                entity.Face.localPosition = new Vector2(_horizontalMovement, 0);
            }

            if ((_deflateSpeed == 1 || _deflateSpeed == -1) && !entity.IsOnCooldown)
            {
                entity.StartCooldown();
                entity.SpawnBubbleLost();
            }
        }
    }
}
