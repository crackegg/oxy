﻿using System;

namespace Assets.Scripts.State.Bubbles
{
    public class DeadBubbleState : State<Bubble>
    {
        private static DeadBubbleState _instance;

        public static DeadBubbleState Instance()
        {
            if (_instance == null)
                _instance = new DeadBubbleState();

            return _instance;
        }

        public override void Enter(Bubble entity)
        {
            AkSoundEngine.PostEvent("BubbleDeath", entity.gameObject);
        }

        public override void Execute(Bubble entity)
        {
            entity.Animator.SetBool("IsDead", true);
        }

        public override void Exit(Bubble entity)
        {
        }

        public override void FixedExecute(Bubble entity)
        {
        }
    }
}
