﻿using Assets.Scripts.State.Bubbles;
using System;
using UnityEngine;

namespace Assets.Scripts.State.Swimmers
{
    public class KeepingOxygenBubbleState : State<Bubble>
    {
        private static KeepingOxygenBubbleState _instance;

        private float _startTime;

        public static KeepingOxygenBubbleState Instance()
        {
            if (_instance == null)
                _instance = new KeepingOxygenBubbleState();

            return _instance;
        }

        public override void Enter(Bubble entity)
        {
            entity.Blush.enabled = false;
            _startTime = Time.time;
        }

        public override void Execute(Bubble entity)
        {
            var t = (Time.time - _startTime) / 2;
            entity.Blush.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(1, 0, t));
        }

        public override void Exit(Bubble entity)
        {
        }

        public override void FixedExecute(Bubble entity)
        {
            if (entity.IsCollidingWithSwimmer)
                entity.StateMachine.ChangeState(GivingOxygenBubbleState.Instance());
        }
    }
}
