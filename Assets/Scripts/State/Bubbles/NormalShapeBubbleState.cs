﻿using UnityEngine;

namespace Assets.Scripts.State.Bubbles
{
    public class NormalShapeBubbleState : State<Bubble>
    {
        private static NormalShapeBubbleState _instance;

        public static NormalShapeBubbleState Instance()
        {
            if (_instance == null)
                _instance = new NormalShapeBubbleState();

            return _instance;
        }

        public override void Enter(Bubble entity)
        {
        }

        public override void Execute(Bubble entity)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                entity.OxygenManager.RemoveEnergyBubble(1);
            }
        }

        public override void Exit(Bubble entity)
        {
        }

        public override void FixedExecute(Bubble entity)
        {
            if (entity.IsCollidingWithSwimmer)
                entity.OxygenManager.AddEnergyToSwimmer();

            var scale = (entity.OxygenManager.CurrentOxygenBubble * 0.1f) + 0.2f;
            entity.transform.localScale = new Vector2(scale, scale);
        }
    }
}
