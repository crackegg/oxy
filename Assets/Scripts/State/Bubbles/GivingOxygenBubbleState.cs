﻿using Assets.Scripts.State.Swimmers;
using UnityEngine;

namespace Assets.Scripts.State.Bubbles
{
    public class GivingOxygenBubbleState : State<Bubble>
    {
        private static GivingOxygenBubbleState _instance;

        private float _startTime;

        public static GivingOxygenBubbleState Instance()
        {
            if (_instance == null)
                _instance = new GivingOxygenBubbleState();

            return _instance;
        }

        public override void Enter(Bubble entity)
        {
            entity.Blush.enabled = true;
            _startTime = Time.time;
        }

        public override void Execute(Bubble entity)
        {
            var t = (Time.time - _startTime) / 2;
            entity.Blush.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(0, 1, t));
        }

        public override void Exit(Bubble entity)
        {
        }

        public override void FixedExecute(Bubble entity)
        {
            if (!entity.IsCollidingWithSwimmer)
                entity.StateMachine.ChangeState(KeepingOxygenBubbleState.Instance());
        }
    }
}
