﻿using UnityEngine;

namespace Assets.Scripts.State.Swimmers
{
    public class InWaterMovementSwimmerState : State<Swimmer>
    {
        private static InWaterMovementSwimmerState _instance;

        private float _horizontalMovement;
        private float _verticalMovement;

        public static InWaterMovementSwimmerState Instance()
        {
            if (_instance == null)
                _instance = new InWaterMovementSwimmerState();

            return _instance;
        }

        public override void Enter(Swimmer entity)
        {
        }

        public override void Execute(Swimmer entity)
        {
            if (_horizontalMovement != 0 || _verticalMovement != 0)
            {
                if (!entity.IsOnCooldown)
                {
                    AkSoundEngine.PostEvent("TadpoleSwims", entity.gameObject);
                    entity.StartCooldown();
                }
                
                entity.Rigidbody.AddForce(new Vector2(-(_horizontalMovement * entity.MaxSpeed * Time.deltaTime), _verticalMovement * entity.MaxSpeed * Time.deltaTime));
                entity.transform.eulerAngles = new Vector3(entity.transform.eulerAngles.x, entity.transform.eulerAngles.y, Mathf.Atan2(_horizontalMovement, _verticalMovement) * Mathf.Rad2Deg);
            }
        }

        public override void Exit(Swimmer entity)
        {
        }

        public override void FixedExecute(Swimmer entity)
        {
            _horizontalMovement = Input.GetAxis("Horizontal_Swimmer");
            _verticalMovement = Input.GetAxis("Vertical_Swimmer");
        }
    }
}
