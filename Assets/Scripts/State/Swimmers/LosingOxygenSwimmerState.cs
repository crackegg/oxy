﻿using System;

namespace Assets.Scripts.State.Swimmers
{
    public class LosingOxygenSwimmerState : State<Swimmer>
    {
        private static LosingOxygenSwimmerState _instance;

        public static LosingOxygenSwimmerState Instance()
        {
            if (_instance == null)
                _instance = new LosingOxygenSwimmerState();

            return _instance;
        }

        public override void Enter(Swimmer entity)
        {
            entity.CapsuleCollider.isTrigger = false;
            AkSoundEngine.PostEvent("TadpoleExitsBubble", entity.gameObject);
        }

        public override void Execute(Swimmer entity)
        {
            if (!entity.IsOnCooldown)
            {
                entity.OxygenManager.RemoveEnergySwimmer();
                entity.StartCooldown();
            }
        }

        public override void Exit(Swimmer entity)
        {
        }

        public override void FixedExecute(Swimmer entity)
        {
            if (entity.IsCollidingWithBubble)
            {
                entity.StateMachine.ChangeState(GainingOxygenSwimmerState.Instance());
            }
        }
    }
}
