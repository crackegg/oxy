﻿using System;

namespace Assets.Scripts.State.Swimmers
{
    public class GainingOxygenSwimmerState : State<Swimmer>
    {
        private static GainingOxygenSwimmerState _instance;

        public static GainingOxygenSwimmerState Instance()
        {
            if (_instance == null)
                _instance = new GainingOxygenSwimmerState();

            return _instance;
        }

        public override void Enter(Swimmer entity)
        {
            entity.CapsuleCollider.isTrigger = true;
            AkSoundEngine.PostEvent("TadpoleEntersBubble", entity.gameObject);
        }

        public override void Execute(Swimmer entity)
        {
            if (!entity.IsOnCooldown)
            {
                entity.OxygenManager.AddEnergyToSwimmer();
                entity.StartCooldown();
            }
        }

        public override void Exit(Swimmer entity)
        {
        }

        public override void FixedExecute(Swimmer entity)
        {
            if (!entity.IsCollidingWithBubble)
                entity.StateMachine.ChangeState(LosingOxygenSwimmerState.Instance());
        }
    }
}
