﻿using System;
using UnityEngine;

namespace Assets.Scripts.State.Swimmers
{
    public class WinnerSwimmerState : State<Swimmer>
    {
        private static WinnerSwimmerState _instance;

        public static WinnerSwimmerState Instance()
        {
            if (_instance == null)
                _instance = new WinnerSwimmerState();

            return _instance;
        }

        public override void Enter(Swimmer entity)
        {
        }

        public override void Execute(Swimmer entity)
        {
            entity.Rigidbody.constraints = UnityEngine.RigidbodyConstraints2D.FreezeRotation;
            entity.transform.eulerAngles = new Vector3(0, 0, 0);
        }

        public override void Exit(Swimmer entity)
        {
        }

        public override void FixedExecute(Swimmer entity)
        {
        }
    }
}
