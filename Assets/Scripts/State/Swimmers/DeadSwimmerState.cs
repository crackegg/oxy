﻿using System;

namespace Assets.Scripts.State.Swimmers
{
    public class DeadSwimmerState : State<Swimmer>
    {
        private static DeadSwimmerState _instance;

        public static DeadSwimmerState Instance()
        {
            if (_instance == null)
                _instance = new DeadSwimmerState();

            return _instance;
        }

        public override void Enter(Swimmer entity)
        {
            AkSoundEngine.PostEvent("TadpoleDeath", entity.gameObject);
        }

        public override void Execute(Swimmer entity)
        {
            entity.Animator.SetBool("IsDead", true);
        }

        public override void Exit(Swimmer entity)
        {
        }

        public override void FixedExecute(Swimmer entity)
        {
        }
    }
}
