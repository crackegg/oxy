﻿using Assets.Scripts.Manager;
using Assets.Scripts.State.Swimmers;
using UnityEngine;

namespace Assets.Scripts
{
    public class Swimmer : BaseGameEntity<Swimmer>
    {
        [HideInInspector]
        public Animator Animator;

        public OxygenManager OxygenManager;
        public CapsuleCollider2D CapsuleCollider;

        public bool IsCollidingWithBubble;
        public bool IsOnCooldown;
        public bool IsDead;

        public Transform Face;
        public Transform DeathFace;

        public void Start()
        {
            Animator = GetComponent<Animator>();
            Rigidbody = GetComponent<Rigidbody2D>();
            CapsuleCollider = GetComponent<CapsuleCollider2D>();

            StateMachine = new StateMachine<Swimmer>(this);
            StateMachine.GlobalState = InWaterMovementSwimmerState.Instance();
            StateMachine.CurrentState = LosingOxygenSwimmerState.Instance();

            OxygenManager.SwimmerDeath = Die;
        }

        private void Die()
        {
            if (IsDead) return;

            IsDead = true;
            StateMachine.GlobalState = DeadSwimmerState.Instance();
            StateMachine.ChangeState(DeadSwimmerState.Instance());
            Face.gameObject.SetActive(false);
            DeathFace.gameObject.SetActive(true);
        }

        public void Desactivate()
        {
            gameObject.SetActive(false);
        }

        public void Update()
        {
            StateMachine.GlobalState.Execute(this);
            StateMachine.CurrentState.Execute(this);
        }

        public void FixedUpdate()
        {
            StateMachine.GlobalState.FixedExecute(this);
            StateMachine.CurrentState.FixedExecute(this);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Bubble"))
                IsCollidingWithBubble = true;
        }
        
        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Bubble"))
                IsCollidingWithBubble = true;
            else
                IsCollidingWithBubble = false;
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Bubble"))
                IsCollidingWithBubble = false;
        }

        public void StartCooldown()
        {
            IsOnCooldown = true;
            Invoke("ResetCooldown", 1f);
        }

        private void ResetCooldown()
        {
            IsOnCooldown = false;
        }
    }
}
