﻿using UnityEngine;
using Assets.Scripts.Manager;

public class Collectible : MonoBehaviour {
    public int BonusAir = 1;
    public float startPush = 2.0f;
    bool hasPushed = false;
    public OxygenManager OxygenManager;

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
        {
            OxygenManager.AddEnergyBubble(BonusAir);
            AkSoundEngine.PostEvent("BubbleAirPickup", gameObject);
            Destroy(gameObject);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Swimmer"))
        {
            AkSoundEngine.PostEvent("TadpoleCollidesAirPickup", gameObject);
        }
    }

    public void Update()
    {
        Vector3 screenPoint = Camera.allCameras[0].WorldToViewportPoint(gameObject.transform.position);
        bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        if (!hasPushed && onScreen)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-startPush, startPush), Random.Range(-startPush, startPush)));
            hasPushed = true;
        }
    }
}
