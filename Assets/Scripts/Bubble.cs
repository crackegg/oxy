﻿using Assets.Scripts.Manager;
using Assets.Scripts.State.Bubbles;
using Assets.Scripts.State.Swimmers;
using UnityEngine;

namespace Assets.Scripts
{
    public class Bubble : BaseGameEntity<Bubble>
    {
        [HideInInspector]
        public CircleCollider2D CapsuleCollider;
        [HideInInspector]
        public SpriteRenderer SpriteRenderer;
        [HideInInspector]
        public Animator Animator;

        public OxygenManager OxygenManager;
        public SpriteRenderer Blush;

        public Transform Face;
        public Transform RightEye;
        public Transform LeftEye;

        public bool IsCollidingWithObstacle;
        public bool IsCollidingWithSwimmer;
        public bool IsOnCooldown;
        public bool IsDead;

        public float ShapeMorphingCooldown;

        public void Start()
        {
            //gameObject.SetActive(true);
            //transform.localScale = new Vector2(0.3f, 0.3f);

            //SpriteRenderer = GetComponent<SpriteRenderer>();
            CapsuleCollider = GetComponent<CircleCollider2D>();
            Rigidbody = GetComponent<Rigidbody2D>();
            Animator = GetComponent<Animator>();

            StateMachine = new StateMachine<Bubble>(this);
            StateMachine.GlobalState = InWaterMovementBubbleState.Instance();
            StateMachine.CurrentState = KeepingOxygenBubbleState.Instance();

            OxygenManager.BubbleDeath = Die;
        }

        public void Desactivate()
        {
            gameObject.SetActive(false);
        }

        private void Die()
        {
            if (IsDead) return;

            StateMachine.GlobalState = DeadBubbleState.Instance();
            StateMachine.CurrentState = DeadBubbleState.Instance();

            Face.gameObject.SetActive(false);
            transform.localScale = new Vector2(0.3f, 0.3f);
            IsDead = true;
        }
        
        public void Update()
        {
            StateMachine.GlobalState.Execute(this);
            StateMachine.CurrentState.Execute(this);
        }

        public void FixedUpdate()
        {
            StateMachine.GlobalState.FixedExecute(this);
            StateMachine.CurrentState.FixedExecute(this);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                IsCollidingWithObstacle = true;

            if (collision.gameObject.layer == LayerMask.NameToLayer("Swimmer"))
                IsCollidingWithSwimmer = true;
        }

        public void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                IsCollidingWithObstacle = false;
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Swimmer"))
                IsCollidingWithSwimmer = true;
        }

        public void OnTriggerExit2D(Collider2D collider)
        {
            if (collider.gameObject.layer == LayerMask.NameToLayer("Swimmer"))
                IsCollidingWithSwimmer = false;
        }

        public void SpawnBubbleLost()
        {
            var go = Instantiate(Resources.Load("BubbleAirLost", typeof(GameObject))) as GameObject;
            go.transform.position = transform.position;
            go.transform.localScale = transform.localScale;
        }

        public void StartCooldown()
        {
            IsOnCooldown = true;
            Invoke("ResetCooldown", 2f);
        }

        private void ResetCooldown()
        {
            IsOnCooldown = false;
        }
    }
}
