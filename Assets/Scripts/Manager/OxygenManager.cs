﻿using System;
using UnityEngine;

namespace Assets.Scripts.Manager
{
    [CreateAssetMenu]
    public class OxygenManager : UnityEngine.ScriptableObject
    {
        public event EventHandler GameOver;

        public Action BubbleDeath;
        public Action SwimmerDeath;
        public Action GameOverGameManager;

        public const int MAX_OXYGEN = 50;
        public int InitialOxygenSwimmer = 50;
        public int InitialOxygenBubble = 50;

        public int CurrentOxygenSwimmer = 0;
        public float CurrentOxygenBubble = 0;

        public bool IsGameOver;

        public void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {
            CurrentOxygenSwimmer = InitialOxygenSwimmer;
            CurrentOxygenBubble = InitialOxygenBubble;
            IsGameOver = false;
        }

        public void AddEnergyToSwimmer()
        {
            if (CurrentOxygenSwimmer >= MAX_OXYGEN)
                return;

            CurrentOxygenSwimmer++;
            RemoveEnergyBubble(1);
        }

        public void AddEnergyBubble(int amount)
        {
            CurrentOxygenBubble = Mathf.Min(MAX_OXYGEN, CurrentOxygenBubble + amount);
        }

        public void RemoveEnergyBubble(float amount)
        {
            CurrentOxygenBubble = Mathf.Max(0, CurrentOxygenBubble - amount);
            CheckIfGameOver();
        }

        public void RemoveEnergySwimmer()
        {
            CurrentOxygenSwimmer = Mathf.Max(0, CurrentOxygenSwimmer - 1);
            CheckIfGameOver();
        }

        public void CheckIfGameOver()
        {
            if (CurrentOxygenBubble == 0)
            {
                BubbleDeath();
            }

            if (CurrentOxygenSwimmer == 0 && !IsGameOver)
            {
                IsGameOver = true;
                SwimmerDeath();
                GameOverGameManager();
            }
        }
    }
}
