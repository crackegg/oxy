﻿using Assets.Scripts.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class OxygenBarSwimmer : MonoBehaviour
    {
        public Image Image;

        public OxygenManager OxygenManager;
        public Animator Animator;
        
        public void Update()
        {
            Image.fillAmount = ((float)OxygenManager.CurrentOxygenSwimmer / OxygenManager.MAX_OXYGEN);
            if (OxygenManager.CurrentOxygenSwimmer <= 0)
                Animator.SetTrigger("Die");

        }
    }
}
