﻿using Assets.Scripts.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class OxygenBarBubble : MonoBehaviour
    {
        public Image Image;

        public OxygenManager OxygenManager;
        public Animator Animator;
        
        public void Update()
        {
            Image.fillAmount = ((float)OxygenManager.CurrentOxygenBubble / OxygenManager.MAX_OXYGEN);
            if (OxygenManager.CurrentOxygenBubble <= 0)
                Animator.SetTrigger("Die");
        }
    }
}
