﻿
using UnityEngine;

namespace Assets.Scripts.UI.Menu
{
    public class QuitMenuTab : MenuTab
    {
        public override void OnClick()
        {
            Application.Quit();
        }
    }
}
