﻿using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.Menu
{
    public class LoadSceneMenuTab : MenuTab
    {
        public int SceneIndexToLoad;

        public override void OnClick()
        {
            AkSoundEngine.PostEvent("StartLevel", gameObject);
            FadeScreenManager.Instance.FadeIn(LoadScene);
        }

        private void LoadScene()
        {
            SceneManager.LoadScene(SceneIndexToLoad);
        }
    }
}
