﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostOpacity : MonoBehaviour {

    private Image _image;

    public void Start()
    {
        _image = GetComponent<Image>();
    }

    public void ReduceOpacity()
    {
        var color = _image.color;
        _image.color = new Color(color.r, color.g, color.b, 0.01f);
    }
}
