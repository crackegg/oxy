﻿using Assets.Scripts.Manager;
using Com.LuisPedroFonseca.ProCamera2D;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public OxygenManager OxygenManager;

    private ProCamera2DTransitionsFX _transition;

    public void Start()
    {
        _transition = GetComponent<ProCamera2DTransitionsFX>();
        _transition.TransitionEnter();
        OxygenManager.GameOverGameManager = GameOver;
    }

    private void GameOver()
    {
        _transition.TransitionExit();
        
        Invoke("Init", _transition.DurationExit - 0.1f);
        Invoke("ReloadScene", _transition.DurationExit);
    }

    private void Init()
    {
        OxygenManager.Initialize();
    }
    
    private void ReloadScene()
    {
        OxygenManager.Initialize();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
