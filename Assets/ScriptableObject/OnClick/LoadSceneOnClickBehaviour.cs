﻿using System;

namespace Assets.ScriptableObject.OnClick
{
    public class LoadSceneOnClickBehaviour : OnClickBehaviour
    {
        public override void Execute(Action wwiseEvent, Action loadScene)
        {
            wwiseEvent();
            loadScene();
        }
    }
}
