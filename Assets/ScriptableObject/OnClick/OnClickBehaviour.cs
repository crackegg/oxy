﻿using System;

namespace Assets.ScriptableObject
{
    public abstract class OnClickBehaviour : UnityEngine.ScriptableObject
    {
        public abstract void Execute(Action wwiseEvent, Action loadScene);
    }
}
