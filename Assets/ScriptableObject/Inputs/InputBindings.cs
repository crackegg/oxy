﻿using UnityEngine;

public class InputBindings : ScriptableObject {

    public string HorizontalInput;
    public string VerticalInput;
    public string JumpInput;
}
