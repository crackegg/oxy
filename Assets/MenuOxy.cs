﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    public class MenuOxy : MonoBehaviour
    {
        public MenuTab[] Tabs;

        private float _cooldown;
        private int _selectedIndex;
        private bool _isOnCooldown;

        public void Update()
        {
            if (_isOnCooldown) return;

            var vertical = Input.GetAxis("Vertical");

            if (vertical < 0)
            {
                if (_selectedIndex == 0)
                    _selectedIndex = Tabs.Length - 1;
                else
                    _selectedIndex--;

                UpdateSelectionVisual();
                StartCooldown();
            }

            if (vertical > 0)
            {
                if (_selectedIndex == Tabs.Length - 1)
                    _selectedIndex = 0;
                else
                    _selectedIndex++;

                UpdateSelectionVisual();
                StartCooldown();
            }

            if (Input.GetButtonDown("Submit"))
            {
                AkSoundEngine.PostEvent("MenuEnterSelection", gameObject);
                Tabs[_selectedIndex].OnClick();
            }
        }

        private void UpdateSelectionVisual()
        {
            AkSoundEngine.PostEvent("MenuMoveCursor", gameObject);
            foreach (var menuTab in Tabs)
            {
                menuTab.Deselect();
            }
            Tabs[_selectedIndex].Select();
        }

        private void StartCooldown()
        {
            _isOnCooldown = true;
            Invoke("ResetCooldown", 0.3f);
        }

        private void ResetCooldown()
        {
            _isOnCooldown = false;
        }
    }
}
