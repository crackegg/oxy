﻿using UnityEngine;

public class BubbleLossAir : MonoBehaviour {

    private Animator _animator;

    public void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void FixedUpdate()
    {
        var currentBaseState = _animator.GetCurrentAnimatorStateInfo(0);

        if (currentBaseState.IsName("Destroy"))
        {
            Destroy(gameObject);
        }
    }
}
