﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitlerEffect : MonoBehaviour {
    public GameObject Bubble;
    public const int LEVEL_HEIGHT = 60;
    private SpriteRenderer render;
	// Use this for initialization
	void Start () {
        render = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        render.color = new Color(0,0, 0, 0.75f - Bubble.transform.position.y / LEVEL_HEIGHT);
	}
}
